const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.vvd8gml.mongodb.net/?retryWrites=true&w=majority")

// number 1
const userSchema = new mongoose.Schema({ 
	username : String,
	password : String
});

// number 2
const User = mongoose.model("User", userSchema); 

app.use(express.json());
app.use(express.urlencoded({extended:true})); 

// number 3
app.post("/signup", (req, res) => {
	let newUser = new User({
		username : req.body.username,
		password : req.body.password
	});

	newUser.save((saveErr, savedUser) => {
		if(saveErr) {
			return console.error(saveErr);
		} else {
			return res.status(200).send("New user created.");
		}
	})
})

app.listen(port, () => console.log(`Server running ${port}`))